const path = require('path');
const fs = require('fs');

function problem2(){

    fs.readFile(path.join(__dirname,"lipsum.txt"),(err,data) => { //Reads the lipsum text file

        if(err){
            console.error("Lipsum file not read",err);
        } else {

            console.log("Lipsum file read");
            let upperCaseText = data.toString().toUpperCase();

            fs.writeFile(path.join(__dirname,"upperCaseFile.txt"),upperCaseText,function(err){ //Writes the uppercase lipsum data to upperCaseFile

                if(err){
                    console.error("UpperCase text could not be written",err);
                } else {

                    console.log("upperCaseFile created");
                    fs.writeFile(path.join(__dirname,"fileNames.txt"),"upperCaseFile.txt",function(err){//Writes the upperCaseFile name to fileNames

                        if(err){
                            console.error("fileNames not created",err);
                        } else {

                            console.log("File fileNames created");
                            fs.readFile(path.join(__dirname,"upperCaseFile.txt"),function(err,data){//Reads the upperCaseFile

                                if(err){
                                    console.error("New file reading failed",err);
                                } else {
                                    
                                    console.log("upperCaseFile read");
                                    let lowerCaseData = data.toString().toLowerCase();
                                    fs.writeFile(path.join(__dirname,"upperCaseFile.txt"),lowerCaseData,function(err){//Writes the lowercase text to upperCaseFile

                                        if(err){
                                            console.error("Conversion to lower case not successful",err);
                                        } else {

                                            console.log("upperCaseFile updated");
                                            fs.readFile(path.join(__dirname,"upperCaseFile.txt"),function(err,data){//Reads the upperCaseFile to convert the data into sentences

                                                if(err){
                                                    console.error("upperCaseFile read failed",err);
                                                } else {

                                                    console.log("upperCaseFile read");
                                                    let paragraphArray = data.toString().split("\n\n");
                                                    let paragraphs = paragraphArray.join();
                                                    let sentencesArray = paragraphs.split(". ");
                                                    let sentences = sentencesArray.join(".\n");

                                                    fs.writeFile(path.join(__dirname,"sentences.txt"),sentences,function(err){//Writes the sentences to the sentences file

                                                        if(err){
                                                            console.error("sentences file not created",err);
                                                        } else {

                                                            console.log("sentences file read");
                                                            fs.appendFile(path.join(__dirname,"fileNames.txt"),"\nsentences.txt",function(err){//Appends the fileNames file with the new filename i.e sentences

                                                                if(err){
                                                                    console.error("fileNames file append failed",err);
                                                                } else {

                                                                    console.log("fileNames appended");
                                                                    fs.readFile(path.join(__dirname,"sentences.txt"),function(err,data){//Reads the sentences file

                                                                        if(err){
                                                                            console.error("sentences file not read",err);
                                                                        } else {

                                                                            console.log("sentences file read");
                                                                            let sortedSentencesArray = sentencesArray.sort()
                                                                            .slice(1);
                                                                            let sortedSentences = sortedSentencesArray.join(".\n");
                                                                            sortedSentences += ".";

                                                                            fs.writeFile(path.join(__dirname,"sortedSentences.txt"),sortedSentences,function(err){//Writes the sorted sentences to the sortedSentences file

                                                                                if(err){
                                                                                    console.error("sortedSentences file not created",err);
                                                                                } else {

                                                                                    console.log("sortedSentences file created");
                                                                                    fs.appendFile(path.join(__dirname,"fileNames.txt"),"\nsortedSentences.txt",function(err){//Appends the fileNames file with the new file name i.e sortedSentences

                                                                                        if(err){
                                                                                            console.error("fileNames not appended",err);
                                                                                        } else {

                                                                                            console.log("fileNames appended");
                                                                                            fs.readFile(path.join(__dirname,"fileNames.txt"),function(err,data){//Reads the fileNames file

                                                                                                if(err){
                                                                                                    console.error("fileNames read failed",err);
                                                                                                } else {

                                                                                                    console.log("fileNames read");
                                                                                                    let fileNamesArray = data.toString().split("\n");

                                                                                                    fileNamesArray.forEach((fileName) => {

                                                                                                        fs.unlink(path.join(__dirname,fileName),function(err) {//Deletes the JSON file
                                                                                                            if(err){
                                                                                                                console.log("File deletion unsuccessful",err);
                                                                                                            } else {
                                                                                                                console.log("File deleted");
                                                                                                            }
                                                                                                        });
                                                                                                    });
                                                                                                }
                                                                                            });
                                                                                        }
                                                                                    });
                                                                                }
                                                                            });
                                                                        }
                                                                    });
                                                                }
                                                            });
                                                        }
                                                    });
                                                }
                                            });
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            });
        }
    });

}

module.exports = problem2;

