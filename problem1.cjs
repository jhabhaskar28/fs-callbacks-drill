const path = require('path');
const fs = require('fs');

function problem1(maximumFileCount = 10){

    fs.mkdir(path.join(__dirname,"randomFiles"),{recursive: true},function(err){//Creates an empty directory randomFiles

        if(err){
            console.error("Directory not created",err);
        } else {

            console.log("Directory created");
            let randomFilesCount = Math.floor(Math.random()*maximumFileCount);

            for(let index = 1; index <= randomFilesCount; ++index){

                fs.writeFile(path.join(__dirname,`randomFiles/file${index}.json`),JSON.stringify(`File name: file${index}.json`),function(err){//Creates random JSON files

                    if(err){
                        console.error("File not created",err);
                    } else {

                        console.log("File created");

                        if(index === randomFilesCount){

                            for(let subIndex = 1; subIndex <= randomFilesCount; ++subIndex){

                                fs.unlink(path.join(__dirname,`randomFiles/file${subIndex}.json`),function(err){//Deletes the JSON files of the randomFiles directory

                                    if(err){
                                        console.error("File deletion failed",err);
                                    } else {
                                        console.log("File deleted");
                                    }
                                });
                            }
                        }
                    }
                });
            }     
        }
    });
}

module.exports = problem1;


